<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio extends CI_Controller {
    
    public function __construct(){
        parent::__construct();
        
        $this->load->model( 'portfolio_model' );
//        $this->load->helper( 'url_helper' );
    }
    
    public function index(){
        
        $data[ 'portfolioPieces' ]
            = $this->portfolio_model->get_portfolio_pieces();
        
        $data[ 'pageTitle' ] = 'Gallery';
        $data[ 'siteTitle' ] = 'Portfolio';
        
		$this->load->view( 'templates/html-top', $data );
        $this->load->view( 'portfolio/gallery', $data );
        $this->load->view( 'templates/html-bottom', $data );
        
    }
    
    public function view( $slug = null ){
        
        $data[ 'piece' ]
            = $this->portfolio_model->get_portfolio_piece( $slug );
        
        if( empty( $data[ 'piece' ] ) ){
            show_404();
        }
        
        $data[ 'pageTitle' ] = $data[ 'piece' ][ 'title' ];
       
        
		$this->load->view( 'templates/html-top', $data );
        $this->load->view( 'portfolio/piece', $data );
        $this->load->view( 'templates/html-bottom', $data );
    }
	
	public function create(){
		$this->load->helper( 'form' );
		$this->load->library( 'form_validation' ); 
		
		
		$data[ 'pageTitle' ] = 'Add Portfolio Piece';
		
		$this->form_validation->set_rules( 'title' , 'Title' , 'required|min_length[3]' );
		
//		$this->form_validation->set_rules( 'image' , 'Image' , 'required' );
		
		$this->form_validation->set_rules( 'description' , 'Description' , 'required|min_length[44]' );
		
		
		if( $this->form_validation->run() === false ){
			
			$this->load->view( 'templates/html-top' , $data ); 
			$this->load->view( 'portfolio/add-form' ); 
			$this->load->view( 'templates/html-bottom' , $data ); 
			
		} else {
			
			$config = array(
				'upload_path' 	=> './images/portfolio/',
				'allowed_types' => '.gif|jpg|png',
				'max-size'		=> '5000',
				'overwrite'		=> true,
				'max_width'		=> 3000,
				'max_height'	=> 3000
				
			);
			
			$this->load->library( 'upload', $config );
			
			if( $this->upload->do_upload( 'image' ) ){
				
			$filename = $this->upload->data( 'file_name' );	
				
			$config = array(
				
					'image_library'	=> 'gd2',
					'source_image'	=> $this->upload->data( 'full_path' ),
					'create_thumb'	=> true,
					'maintain_ratio'=> true,
					'width'			=> 300
					
				
			);	
				
				
			$this->load->library( 'image_lib', $config );	
				
			$this->image_lib->resize();	
				
			$this->portfolio_model->add_portfolio_piece( $filename );
				
			$this->load->view( 'templates/html-top' , $data ); 
			$this->load->view( 'portfolio/add-success' ); 
			$this->load->view( 'templates/html-bottom' , $data ); 
				
				
			} else {
				
			$data[ 'uploadError' ] = $this->upload->display_errors();	
				

			$this->load->view( 'templates/html-top' , $data ); 
			$this->load->view( 'portfolio/add-form', $data ); 
			$this->load->view( 'templates/html-bottom' , $data ); 
				
				
				
			}
			
			
			
			
			
		}
		
		
       
		
	}
	
}
