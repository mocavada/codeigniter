<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {
	
	
//	public function __construct(){
//		parent::__construct();
//		$this->load->helper( 'url_helper' );
//		
//	}
    
    public function view( $page = 'about' ){
                
        if( !file_exists( APPPATH . 'views/pages/' . $page . '.php' ) ){
            show_404();
        }
        
        $data[ 'pageTitle' ] = ucfirst( $page );
        
        
		$this->load->view( 'templates/html-top', $data );
        $this->load->view( 'pages/' . $page, $data );
        $this->load->view( 'templates/html-bottom', $data );
    }
}