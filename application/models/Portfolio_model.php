<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Portfolio_model extends CI_Model{

    public function __construct(){
        $this->load->database();
    }

    public function get_portfolio_pieces(){
        $query = $this->db->get( 'portfolio_pieces' );
		
		
		
		$pieces = $query->result_array();
		$newPieces = array();
		
		foreach( $pieces as $piece ){
			
			$fileParts = explode( '.', $piece[ 'image' ] );
			$extension = array_pop( $fileParts );
			
			$piece[ 'thumb' ] = implode( '.' , $fileParts )
										 . '_thumb'
										 . '.' . $extension;
			
			array_push( $newPieces, $piece );
			
			
		}
		
        return $newPieces;
    }
    
	
	
    public function get_portfolio_piece( $slug ){
        $query 
            = $this->db->get_where( 'portfolio_pieces',
                                    array( 'slug' => $slug ) );
        
        return $query->row_array();
    }
	
	public function add_portfolio_piece( $filename ){
		
		$this->load->helper( 'url' );
		
		$slug = url_title( $this->input->post( 'title' ) , '-' , true );	
		$data = array(
			
				'title' => $this->input->post( 'title'),
				'slug' 	=> $slug,
				'image'	=> $filename,
				'description'	=> $this->input->post( 'description' )
		
		);
			
			return $this->db->insert( 'portfolio_pieces', $data );
	}
}