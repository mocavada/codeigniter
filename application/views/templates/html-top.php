<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title><?php echo $pageTitle; ?> - <?php echo SITE_TITLE; ?></title>
        
        <!-- link to the main stylesheet -->
        <link rel="stylesheet" href="<?php echo base_url( 'css/style.css'); ?>" />
        
        <!--[if lt IE 9]>
	    <script src="js/html5shiv.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            <h1>
                <a href="/"><?php echo SITE_TITLE; ?></a>
            </h1>
        </header>
		<?php $this->load->view( 'templates/navigation' ); ?>
        <main>
            <h2><?php echo $pageTitle; ?></h2>