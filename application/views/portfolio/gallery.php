<ol id="gallery">
    <?php foreach( $portfolioPieces as $piece ): ?>
    <li class="col14 fl">
        <a href="<?php echo site_url( 'portfolio/' . $piece[ 'slug' ] );?>">
            <h3><?php echo $piece[ 'title' ]; ?></h3>
            <img src="<?php 
                echo base_url( 'images/portfolio/' . $piece[ 'thumb' ] );      
                 ?>" alt="<?php echo $piece[ 'title' ]; ?>" />
        </a>
    </li> 
    <?php endforeach; ?>
</ol>